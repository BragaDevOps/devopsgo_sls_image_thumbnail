package service

import (
	"bitbucket.org/BragaDevOps/sls_devopsgo_image_thumbnail_sqs/types"
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/disintegration/imaging"
	"github.com/sirupsen/logrus"
	"github.com/subosito/gotenv"
	"image"
	"image/color"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

type Service struct {
	aws       types.AWSProvider
	param     types.LambdaParams
	s3Records events.S3Event
}

var invokeCount = 0
var invalidFormatList []map[string]interface{}

func init() {
	_ = gotenv.Load()
}

func NewService(awsProvider types.AWSProvider, lambdaParams types.LambdaParams) *Service {
	return &Service{
		aws:   awsProvider,
		param: lambdaParams,
	}
}

func (s *Service) Start() {
	if err := s.validateParams(); !err {
		os.Exit(1)
	}
	s.aws.Initialize(s.Make)
}

func (s *Service) Make(ctx context.Context, sqsEvent *events.SQSEvent) (int, error)  {
	invokeCount = invokeCount + 1

	if err := s.populateRecords(sqsEvent); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "Make",
		}).Error("Error in populateRecords:")

		return invokeCount, err
	}

	if err := s.handleRecords(); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "Make",
		}).Error("Error in handleRecords:")

		return invokeCount, err
	}

	byte, _ := json.Marshal(invalidFormatList)
	if byte == nil {
		logrus.WithFields(logrus.Fields{
			"List": byte,
		}).Warn("Invalid format list:")
	}

	return invokeCount, nil
}

func (s *Service) populateRecords(sqsEvent *events.SQSEvent) error {
	if len(sqsEvent.Records) > 0 {
		snsEntity := events.SNSEntity{}
		if err := json.Unmarshal([]byte(sqsEvent.Records[0].Body), &snsEntity); err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"file": "service/service.go",
				"func": "populateRecords",
			}).Error("Error while Unmarshal for struct events.SNSEntity:")

			return err
		}

		s.s3Records = events.S3Event{}
		if err := json.Unmarshal([]byte(snsEntity.Message), &s.s3Records); err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"file": "service/service.go",
				"func": "populateRecords",
			}).Error("Error while Unmarshal for struct events.S3Event{}:")

			return err
		}
	}

	return nil
}

func (s *Service) handleRecords() error {
	if len(s.s3Records.Records) > 0 {
		s3Record      := s.s3Records.Records[0]
		bucket        := s3Record.S3.Bucket.Name
		key           := s3Record.S3.Object.Key
		imageNamePath := s.param.ThumbPathSave + bucket + "/" + key

		file, err := s.createLocalFile(imageNamePath, bucket, key)
		if err != nil {
			return err
		}

		s.aws.SetConfig(&aws.Config{
			Region: aws.String(os.Getenv("AWS_REGION")),
		})
		s.aws.NewSession()

		if err := s.aws.DownloadObjectS3(file, bucket, key); err != nil {
			return err
		}

		if err := s.validateImage(imageNamePath); !err {
			invalidFormat := map[string]interface{}{
				"error": "Invalid format image",
				"imageNamePath": imageNamePath,
			}
			invalidFormatList = append(invalidFormatList, invalidFormat)

			return nil
		}

		if err := s.generateThumb(imageNamePath, bucket, key); err != nil {
			return err
		}

		if err := s.aws.UploadToS3(imageNamePath, s.param.BucketThumbS3Name, key); err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) createLocalFile(imageNamePath, bucket, key string) (*os.File , error) {

	// Ensure path is available
	dir := filepath.Dir(imageNamePath)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "createLocalFile",
		}).Error("Failed to create directory:")
		return nil, err
	}

	// Create a file locally for original image in S3
	file, err := os.Create(imageNamePath)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "createLocalFile",
		}).Error("Failed to create file:")
		return nil, err
	}

	return file, nil
}

func (s *Service) generateThumb(imageNamePath, bucket, key string) error {

	imageOpen, err := imaging.Open(imageNamePath)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "generateThumb",
		}).Error("Failed to opem file:")
		return err
	}
	thumb := imaging.Thumbnail(imageOpen, s.param.ThumbWidth, s.param.ThumbHeight, imaging.CatmullRom)

	// Create a new blank image
	dst := imaging.New(s.param.ThumbWidth, s.param.ThumbHeight, color.NRGBA{0, 0, 0, 0})

	// Paste thumbnails into the new image
	dst = imaging.Paste(dst, thumb, image.Pt(0, 0))
	err = imaging.Save(dst, imageNamePath)
	if err != nil {
		logrus.WithError(err).WithField("thumbnail", imageNamePath).Error("Failed to generate thumbnail:")
		return err
	}

	return nil
}

func (s *Service) validateImage(imageNamePath string) bool {

	file, err := os.Open(imageNamePath)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "validateImage",
		}).Error("Failed to opem file:")
		return false
	}

	// http://golang.org/pkg/net/http/#DetectContentType
	buff := make([]byte, 512)
	_, err = file.Read(buff)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "validateImage",
		}).Error("Failed to read image:")
		return false
	}

	imageType := http.DetectContentType(buff)

	switch imageType {
	case "image/jpeg", "image/jpg":
		return true

	case "image/gif":
		return true

	case "image/png":
		return true

	default:
		logrus.WithError(err).WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "validateImage",
		}).Error("File type not supported:")

		return false
	}
}

func (s *Service) validateParams() bool {

	bucketImgS3Name := os.Getenv("AWS_BUCKET_IMG_S3")
	if len(bucketImgS3Name) == 0 {
		logrus.WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "validateParams",
		}).Info("AWS_BUCKET_IMG_S3 parameter cannot be empty:")

		return false
	}
	s.param.BucketImgS3Name = bucketImgS3Name

	bucketThumbS3Name := os.Getenv("AWS_BUCKET_THUMB_S3")
	if len(bucketThumbS3Name) == 0 {
		logrus.WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "validateParams",
		}).Info("AWS_BUCKET_THUMB_S3 parameter cannot be empty:")

		return false
	}
	s.param.BucketThumbS3Name = bucketThumbS3Name

	awsAccountRegion := os.Getenv("AWS_ACCOUNT_REGION")
	if len(awsAccountRegion) == 0 {
		logrus.WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "validateParams",
		}).Info("AWS_ACCOUNT_REGION parameter cannot be empty.:")

		return false
	}
	s.param.AwsAccountRegion = awsAccountRegion

	thumbWidth := os.Getenv("THUMB_WIDTH")
	if len(thumbWidth) == 0 {
		logrus.WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "validateParams",
		}).Info("AWS_BUCKET_THUMB_S3 parameter cannot be empty:")

		return false
	}
	s.param.ThumbWidth, _ = strconv.Atoi(thumbWidth)

	thumbHeight := os.Getenv("THUMB_HEIGTH")
	if len(thumbHeight) == 0 {
		logrus.WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "validateParams",
		}).Info("AWS_BUCKET_THUMB_S3 parameter cannot be empty:")

		return false
	}
	s.param.ThumbHeight, _ = strconv.Atoi(thumbHeight)

	thumbPathSave := os.Getenv("THUMB_PATH_SAVE")
	if len(thumbPathSave) == 0 {
		logrus.WithFields(logrus.Fields{
			"file": "service/service.go",
			"func": "validateParams",
		}).Info("AWS_BUCKET_THUMB_S3 parameter cannot be empty:")

		return false
	}
	s.param.ThumbPathSave = thumbPathSave

	return true
}


