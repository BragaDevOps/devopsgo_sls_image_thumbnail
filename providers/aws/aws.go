package aws

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/sirupsen/logrus"
	"os"
	"context"
)

var awsSession *session.Session

type ProviderAWS struct {
	awsConfig   *aws.Config
	handlerFunc func (ctx context.Context, sqsEvent *events.SQSEvent) (int, error)
}

func (p *ProviderAWS) SetConfig(config *aws.Config) {
	p.awsConfig = config
}

func (p *ProviderAWS) NewSession() {
	awsSession = session.Must(session.NewSession(p.awsConfig))
}

func (p *ProviderAWS) Initialize(handler func (ctx context.Context, sqsEvent *events.SQSEvent) (int, error)) {
	p.handlerFunc = handler
	lambda.Start(p.make)
}

func (p *ProviderAWS) make(ctx context.Context, sqsEvent *events.SQSEvent) (int, error)  {

	invokeCount, err := p.handlerFunc(ctx, sqsEvent)
	if err != nil {
		return invokeCount, err
	}

	logrus.WithFields(logrus.Fields{
		"invokeCount": invokeCount,
	}).Info("Number of times lambda was invoked:")

	return invokeCount, nil
}

func (p *ProviderAWS) DownloadObjectS3(file *os.File, bucket, key string) error {
	s3NewDownloader := s3manager.NewDownloader(awsSession)
	numBytes, err := s3NewDownloader.Download(file, &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"bucket":   bucket,
			"key":      key,
			"filename": file.Name(),
		}).Error("Failed to download file")

		return err
	}

	logrus.WithFields(logrus.Fields{
		"filename": file.Name(),
		"bytes":    numBytes,
	}).Info("File downloaded:")

	return nil
}

func (p *ProviderAWS) UploadToS3(tmpImagePath, bucket, key string) error {
	upFile, err := os.Open(tmpImagePath)
	if err != nil {
		logrus.WithError(err).WithField("UploadToS3", tmpImagePath).Error("Failed to open file:")
		return err
	}
	defer upFile.Close()

	s3NewUploader := s3manager.NewUploader(awsSession)
	result, err := s3NewUploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   upFile,
	})
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"bucket": bucket,
			"key":    key,
		}).Error("Failed to upload file:")
		return err
	}

	logrus.WithField("UploadToS3", result.Location).Info("File uploaded:")

	return nil
}
