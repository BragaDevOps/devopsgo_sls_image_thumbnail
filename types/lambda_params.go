package types

type LambdaParams struct {
	Region             string   `json:"region"`
	BucketImgS3Name    string   `json:"bucket_image_s3_name"`
	BucketThumbS3Name  string   `json:"bucket_thumb_s3_name"`
	AwsAccountRegion   string   `json:"aws_account_region"`
	ImageTypes         []string `json:"image_types"`
	ThumbWidth         int      `json:"thumb_width"`
	ThumbHeight        int      `json:"thumb_height"`
	ThumbPathSave      string   `json:"thumb_path_save"`
}
