package main

import (
	"bitbucket.org/BragaDevOps/sls_devopsgo_image_thumbnail_sqs/providers/aws"
	"bitbucket.org/BragaDevOps/sls_devopsgo_image_thumbnail_sqs/service"
	"bitbucket.org/BragaDevOps/sls_devopsgo_image_thumbnail_sqs/types"
)

func main(){
	providerAWS  := &aws.ProviderAWS{}
	lambdaParams := types.LambdaParams{}
	srv          := service.NewService(providerAWS, lambdaParams)
	srv.Start()
}
